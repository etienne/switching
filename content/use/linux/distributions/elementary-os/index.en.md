---
title: elementary OS
icon: icon.svg
replaces: 
    - windows
    - mac-os
---

**elementary OS** is an Ubuntu-based distribution with a focus on non-technical users. Its user interface resembles the one of macOS.

{{< infobox >}}
- **Website:**
    - [elementary.io](https://elementary.io)
    - [Screenshots](https://linuxmint.com/screenshots.php)
- **Download:**
    - [Linux Mint](https://linuxmint.com/download.php)
{{< /infobox >}}