---
title: Thunderbird
icon: icon.png
replaces: 
    - microsoft-office
---
The most popular [open source][floss] email app for computers is Thunderbird. While primarily focused on email management, Thunderbird also comes with some extra features such as: viewing [RSS feeds][rss], [calendar extension][calendar], events/tasks list, and integration with some external chat services.

{{< infobox >}}
- **Website**:
  [Mozilla Thunderbird](https://www.thunderbird.net)
{{< /infobox >}}

[calendar]: https://www.thunderbird.net/en-US/calendar/
[floss]: https://web.archive.org/web/20180904102804/https://switching.social/what-is-open-source-software/
[rss]: {{< relref "/use/rss" >}}