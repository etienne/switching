---
slug: riot
title: Riot / Matrix
icon: icon.svg
replaces:
    - whatsapp
    - facebook-messenger
    - slack
---

**Riot.im** is a libre instant messaging client based on the Matrix protocol. It includes text chats, audio/video calls and file transfers.

**The Matrix protocol** has a federated design and allows bridges to other communication apps. Furthermore, anyone can set up their own Matrix server and use it to collaborate with other people’s Matrix servers.

{{< infobox >}}
- **Website:** 
    - [Riot.im App](https://riot.im/app)
    - [Matrix Project](https://matrix.org/)
- **Desktop app:**
    - [Windows / macOS / Linux](https://riot.im/download/desktop/)
- **Mobile app:**
    - [Android](https://play.google.com/store/apps/details?id=im.vector.app) ([FDroid](https://f-droid.org/en/packages/im.vector.alpha/))
    - [iOS](https://itunes.apple.com/us/app/vector.im/id1083446067)
- **Explore:**
    - [Matrix Clients](https://matrix.org/clients/)
{{< /infobox >}}

[floss]: https://web.archive.org/web/20180904102804/https://switching.social/what-is-open-source-software/
