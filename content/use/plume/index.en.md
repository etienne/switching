---
title: Plume
icon: icon.svg
replaces:
    - medium
---

**Plume** is an [open source][floss] [federated][fediverse] blogging platform, which lets you create a federated blog. This means it can be followed from Mastodon and other social networks powered by the ActivityPub standard.

{{< infobox >}}
- **Sign up:** 
    - [fediverse.blog](https://fediverse.blog/)
    - [plume.mastodon.host](https://plume.mastodon.host/)
    - [cafe.sunbeam.city](https://cafe.sunbeam.city/)
    - and many others
- **Website:**
    - [Joinplu.me](https://joinplu.me/)
{{< /infobox >}}

[fediverse]: {{< relref "/articles/federated-sites" >}}
[floss]: {{< relref "/articles/free-libre-open-software" >}}