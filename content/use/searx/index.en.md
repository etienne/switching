---
title: searx
icon: icon.png
replaces: 
    - google-search
---

**Searx** is an open source “metasearch” engine, which searches several other search engines without telling them anything about you.

There are many sites that run Searx, you can use any of them (or if you’re technically minded you can even start your own site).

{{< infobox >}}
- **Websites:**
    - [searx.me](https://searx.me/)
    - [search.disroot.org](https://search.disroot.org/)
    - [searx.info](https://searx.info/)
    - [searx.site](https://searx.site/)
    - [searx.xyz](https://searx.xyz/)
{{< /infobox >}}
