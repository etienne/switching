---
title: Jitsi Meet
icon: icon.svg
replaces: 
    - skype
---

**Jitsi Meet** is a [free open source][floss] video chat app made by volunteers. You can use it through their website or through the mobile apps.

It is extremely easy to use: you don’t need to register, all you need to do is make up the name of a chat room and send the link to whoever you want to join you. You can also add a room password if you want. 

{{< infobox >}}
- **Website:**
    - [Jitsi Meet](https://meet.jit.si/)
- **Mobile app:**
    - [Android](https://play.google.com/store/apps/details?id=org.jitsi.meet) ([FDroid](https://f-droid.org/en/packages/org.jitsi.meet/))
    - [iOS](https://itunes.apple.com/us/app/jitsi-meet/id1165103905)
{{< /infobox >}}

[floss]: {{< relref "/articles/free-libre-open-software" >}}